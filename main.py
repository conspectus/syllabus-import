import json
import os
import sys
import pandas as pd

from net import *

url_syllabus = "http://localhost:8142/conspectus/citation"
directory_to_use = './'
headers = {'Content-type': 'application/json',
           'Authorization': 'Basic YWRtaW5fcG9zdDpTRVRNRVRPU09NRVRISU5H'}

def main(argv):

    files = os.listdir(directory_to_use)
    for syllabus_file in files:
        citations = pd.read_csv(syllabus_file)
        citations_as_jsons = citations.to_json(orient='records', lines=True).splitlines()
        for citation in citations_as_jsons:
            citation_as_json = json.loads(citation)
            response = do_post_request(url_syllabus, headers, citation)
            if response.status == 201:
                logging.info('Created citation with citationId {}'.
                             format(citation_as_json['citation_id']))
            elif response.status == 409:
                logging.info('Citation with citationId {} already exists in API'.
                             format(citation_as_json['citation_id']))
            else:
                logging.error('Error writing citation [{}] to URL[%2], status [{}]'.
                              format(citation_as_json['citation_id'], url_syllabus, response.status))

if __name__ == '__main__':
    main(sys.argv[1:])


